# s3-cloudfront-site

## Purpose

This module provisions static site hosting in AWS

## Description

Provisions S3, CloudFront, IAM resources necessary to host static website in AWS.

![architectural diagram](images/s3-cloudfront-site.png)

## Usage Instructions

Copy and paste into your Terraform configuration, insert or update the
variables, and run `terraform init`:

```
module "s3-cloudfront-site" {
  source                   = "git@code.vt.edu:devcom/terraform-modules/aws-s3-cloudfront-site.git"
  certificate_arn          = "arn:aws:acm:us-east-1:<Account_Number>:certificate/45c8031c-e4d8-45c8-a99e-e859f02679dc"
  ci_username              = "sitename"
  environment              = "prod"
  FQDN                     = "sitename.it.vt.edu"     
  responsible_party        = "sally"
  vcs                      = "git@code.vt.edu:path/to/my/repo"
  versioning_enabled       = true
  ...
}
```

## Preconditions and Assumptions

The following needs to exist in AWS already:

* ACM certificate with both the FQDN and www.FQDN names on the cert

## Dependencies

* TBD

## Inputs

### Required Inputs
These variables are required.

| Name | Type | Description |
| ---- | ---- | ----------- |
| **certificate_arn** | `[string]` | Amazon Resource Name (arn) for the site's certificate |
| **ci_username** | `[string]` |  IAM user for CI/CD pipeline to access S3 bucket and Cloudfront cache invalidation |
| **FQDN** | `[string]` | Gitlab token for your group or project |
| **responsible_party** | `[string]` | person (pid) who is primarily responsible for the configuration and maintenance of this resource
| **vcs** | `[string]` | A link to the repo in a version control system (usually Git) that manages this resource.

### Optional Inputs
These variables have default values and don't have to be set to use this module.
You may set these variables to override their default values.


| Name | Type | Description | Default |
| ---- | ---- | ----------- | ------- |
| **compliance_risk** | `[string]` | should be `none`, `ferpa` or `pii` | `none`
| **data_risk** | `[string]` | should be `low`, `medium` or `high` based on data-risk classifications defined [here](http://it.vt.edu/content/dam/it_vt_edu/policies/Virginia-Tech-Risk-Classifications.pdf) | `low`
| **documentation** | `[string]` | link to documentation and/or history file | `none`
| **environment** | `[string]` | e.g. `development`, `test`, or `production` | `prod`
| **error_document** | `[string]` | Error page being served from S3 bucket | `error.html`
| **index_document** | `[string]` | Home page being served from S3 bucket | `index.html`
| **minimum_protocol_version** | `[string]` | Security policy for cloudfront to use for HTTPS connections | `TLSv1`
| **responsible_party2** | `[string]` | Backup for responsible_party | `none`
| **route53_hosted_zone_id** | `[string]` | Route53 Hosted Zone ID to use for creation of records pointing to CloudFront distribution
| **service_name** | `[string]` | The high level service this resource is primarily supporting | `static-site-hosting`
| **skip_www_alias** | `[boolean]` | Skip the inclusion of the www.FQDN alias on the distribution
| **versioning_enabled** | `[boolean]` | Versioning for the objects in the S3 bucket | `false`


## Outputs

The following outputs are exported by the module. Each of the outputs is an object that provides additional details. As an example, if you defined the module using the example above, you could access the name of the created S3 bucket by using `module.s3-cloudfront-site.s3_bucket.name`.

| Name | Description | Structure |
| ---- | ----------- | --------- |
| **s3_bucket** | Details of the S3 bucket created to host the site content | <ul><li><strong>name</strong> - the name of the bucket</li><li><strong>arn</strong> - the arn of the bucket</li></ul>
| **ci_user** | Details about the IAM user created to allow bucket syncing/distribution invalidation in CI builds | <ul><li><strong>name</strong> - the name of the user</li><li><strong>arn</strong> - the arn of the user</li></ul>
| **cloudfront_distribution** | Details about the CloudFront distribution created to serve the site content | <ul><li><strong>id</strong> - the id of the distribution</li><li><strong>arn</strong> - the arn of the distribution</li><li><strong>domain_name</strong> - the domain name of the distribution</li><li><strong>hosted_zone_id</strong> - the hosted zone ID for the distribution</li><li><strong>aliases</strong> - the aliases applied to the distribution</ul>


## Versions

| Version | Major changes |
| ------- | ------------- |
| 1     | Created module |

## TODO

- TBD