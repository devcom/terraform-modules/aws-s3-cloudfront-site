resource "aws_route53_record" "site" {
  count = var.route53_hosted_zone_id != "" ? length(local.domain_aliases) : 0

  zone_id = var.route53_hosted_zone_id
  name    = element(local.domain_aliases, count.index)
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "site_v6" {
  count = var.route53_hosted_zone_id != "" ? length(local.domain_aliases) : 0

  zone_id = var.route53_hosted_zone_id
  name    = element(local.domain_aliases, count.index)
  type    = "AAAA"

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}
